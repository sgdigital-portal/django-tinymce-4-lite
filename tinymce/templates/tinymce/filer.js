function tinyMCEDjangoFilerCallback(field_name, input_value, type, win){
    var url = "{{ admin_filer_url }}?_pick=file&_popup=1";

    var windowname = "$TinyMCE$" + field_name + "$" + tinyMCE.activeEditor.id;
    var win = window.open(url, windowname, "width=840,height=600,resizable=yes,scrollbars=yes");
    win.focus();
    return false;
}

(function ($) {
    filer_dismiss = window.dismissRelatedImageLookupPopup

    window.dismissRelatedImageLookupPopup = function (win, chosenId, chosenThumbnailUrl, chosenDescriptionTxt) {
        if (win.name.indexOf("$TinyMCE$") === 0) {
            var vars = win.name.replace("$TinyMCE$", "");
            vars = vars.split("$");
            var field_name = vars[0];
            var editor_id = vars[1];
            var editor = tinyMCE.get(editor_id);

            var filer_canonical_url = "{% url "filer_canonical" 1337 %}";

            file_url = filer_canonical_url.replace('1337', chosenId) + chosenDescriptionTxt;

            $('#' + field_name).val(file_url);
            win.close();
        } else {
            return filer_dismiss(win, chosenId, chosenThumbnailUrl, chosenDescriptionTxt)
        }
    };
})(django.jQuery);

